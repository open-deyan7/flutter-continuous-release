lane :slack_internal_release_message do |options|
  slack(
      pretext: options[:pretext],
      message: options[:message],
      channel: "#app",
      fail_on_error: "false",
      default_payloads: ["test_result", "git_branch", "last_git_commit", "last_git_commit_hash"],
      attachment_properties: {
       thumb_url: options[:thumb_url],
       fields: [
       {
         title: 'Build Number',
         value: ENV['BUILD_NUMBER'],
         short: true
       },
       {
         title: 'App Version',
         value: ENV['APP_VERSION'],
         short: true
       }]
      },
      slack_url: ENV['SLACK_WEBHOOK_URL']
    )
end

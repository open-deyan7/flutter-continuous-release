# reads GITLAB ENV variable containing base 64 encoded android play console service account credentials and stores it as a file which is accessible for fastlane
echo $PLAY_STORE_CREDENTIALS_BASE_64 | base64 -d > ./android/fastlane/play-store-credentials.json
echo $ANDROID_KEYSTORE_FILE_BASE64 | base64 -d > ./android/app/keystore.jks

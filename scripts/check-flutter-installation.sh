brew install jq
FLUTTER_VERSION=$(grep -i "image:.*cirrusci/flutter" .gitlab-ci.yml | cut -d ':' -f 3)
CURRENT_FLUTTER_VERSION=$(flutter --version --machine | jq .frameworkVersion | tr -d '"')
if [ "$CURRENT_FLUTTER_VERSION" = "$FLUTTER_VERSION" ]; then
  echo "Flutter Version is $FLUTTER_VERSION (correct!)"
  else
    echo "Flutter Version must be $FLUTTER_VERSION"
    exit 1
fi

echo "Checking flutter doctor..."
FLUTTER_DOCTOR_RESULT=$(flutter doctor)
if [[ $FLUTTER_DOCTOR_RESULT == *"No issues found!"* ]]; then
  echo "$FLUTTER_DOCTOR_RESULT"
  echo "Flutter doctor reports no issues"
  else
    echo "Flutter doctor reports issues"
    echo "$FLUTTER_DOCTOR_RESULT"
    exit 1
fi

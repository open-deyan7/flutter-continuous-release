export APP_VERSION=$(cat pubspec.yaml| grep "version: " | cut -d ":" -f 2 | cut -d " " -f 2 | cut -d "+" -f 1)
echo 'app version is '$APP_VERSION

export BUILD_NUMBER=$(git rev-list HEAD --count)
echo 'using build number '$BUILD_NUMBER
